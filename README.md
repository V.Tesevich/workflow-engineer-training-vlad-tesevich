# Tesevich Vlad

## I 18 years old. I studying in MRC(Minsk radio engineering college)

## I learn python more than 1 year. I am currently doing an internship at Syberry

## I like to play the guitar and spend a lot of time on it

## *My experience*
I wrote 5 small projects in python.<br>
Music player(PyQt5), dynamic currency parser(BS4),<br>
django tourizm site(Django), forum(DRF, PostgreSql),<br>
Tinder API(DRF, GeoDjago, PostGis, JWT)

**My technology skills:**
- python - 3
- mysql - 2
- yaml - 1
- csv - 1
- md - 1
- git - 2
- redmine - 0
- jira - 0

## Working hours
weekdays ***from 8.00 to 12.00*** and ***19.00 - 24.00***<br>
weekends from ***10.00 to 20.00***

## Links
**GitHub <https://github.com/ByAvatarOff>**<br>
**GitLab <https://gitlab.com/V.Tesevich>**<br>
**Jira <https://workflow-engineers-syberry.atlassian.net/jira/people/604b924203ceda0069c92b31>**<br>
**Redmine <https://www.redmine.org/users/512324>**<br>
**Github <https://github.com/ByAvatarOff>**<br>

